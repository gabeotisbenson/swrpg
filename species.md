# Species
## Bothan
**Wound Threshold** - 10 + Brawn

**Strain Threshold** - 11 + Willpower

**Starting XP** - 100

### Characteristics
- Brawn - 1
- Agility - 2
- Intellect - 2
- Cunning - 3
- Willpower - 2
- Presence - 2

### Special Abilities
- Start with one rank in Streetwise skill
- Start with one rank in Convincing Demeanor talent

## Cerean
**Wound Threshold** - 10 + Brawn

**Strain Threshold** - 13 + Willpower

**Starting XP** - 90

### Characteristics
- Brawn - 2
- Agility - 1
- Intellect - 3
- CUnning - 2
- Willpower - 2
- Presence - 2

### Special Abilities
- Start with one rank in Vigilance skill
- Treat all Knowledge skills as career skills

## Droid
**Wound Threshold** - 10 + Brawn

**Strain Threshold** - 11 + Willpower

**Starting XP** - 175

### Characteristics
- Brawn - 1
- Agility - 1
- Intellect - 1
- Cunning - 1
- Willpower - 1
- Presence - 1

### Special Abilities
- Do not need to eat, sleep, or breathe
- Unaffected by toxins or poisons
- Cybernetic implant cap of 6
- May gain one rank in 6 of 8 career skills instead of 4
- May gain one rank in 3 of 4 specialization skills instead of 2
- Cannot heal from bacta, stimpack, or Medicine skill checks
- Naturally recover from resting (self-repairs)
- Heal from Mechanics check or Emergency repair patches
- Start with one rank in Enduring talent
- Cannot be Force sensitive
- Cannot be affected by mind-altering Force powers

## Duros
**Wound Threshold** - 11 + Brawn

**Strain Threshold** - 10 + Willpower

**Starting XP** - 100

### Characteristics
- Brawn - 1
- Agility - 2
- Intellect - 3
- Cunning - 2
- Willpower - 2
- Presence - 2

### Special Abilities
- Start with one rank in Piloting (Space) skill
- Add one advantage to all Astrogation checks

## Gand
**Wound Threshold** - 10 + Brawn

**Strain Threshold** - 10 + Willpower

**Starting XP** - 100

### Characteristics
- Brawn - 2
- Agility - 2
- Intellect - 2
- Cunning - 2
- Willpower - 3
- Presence - 1

### Special Abilities
- Start with one rank in Discipline skill
- If lungless variant, immune to suffocation
- If lungs, start with ammonia respirator and treat oxygen as dangerous
	atmosphere with Rating 8.  Gain +10 starting XP.

## Gran
**Wound Threshold** - 10 + Brawn

**Strain Threshold** - 9 + Willpower

**Starting XP** - 100

### Characteristics
- Brawn - 2
- Agility - 2
- Intellect - 2
- Cunning - 1
- Willpower - 2
- Presence - 3

### Special Abilities
- Start with one rank in Charm or Negotiation skills
- When making ranged combat or Perception checks, Gran remove up to two setback
	imposed due to environmental conditions or concealment (but not defense)

## Human
**Wound Threshold** - 10 + Brawn

**Strain Threshold** - 10 + Willpower

**Starting XP** - 110

### Characteristics
- Brawn - 2
- Agility - 2
- Intellect - 2
- Cunning - 2
- Willpower - 2
- Presence - 2

### Special Abilities
- Start with one rank in two non-career skills

## Ithorian
**Wound Threshold** - 9 + Brawn

**Strain Threshold** - 12 + Willpower

**Starting XP** - 90

### Characteristics
- Brawn - 2
- Agility - 1
- Intellect - 2
- Cunning - 2
- Willpower - 3
- Presence - 2

### Special Abilities
- Start with one rank in Survival
- Natural unique weapon (bellow) that causes 3 strain

## Kel Dor
**Wound Threshold** - 10 + Brawn

**Strain Threshold** - 10 + Willpower

**Starting XP** - 100

### Characteristics
- Brawn - 1
- Agility - 2
- Intellect - 2
- Cunning - 2
- Willpower - 3
- Presence - 2

### Special Abilities
- Start with one rank in Knowledge (Education)
- When making skill checks remove up to two setback imposed due to darkness
- Must wear a specialized mask to breathe and see outside their native
	atmosphere
- Start with an antitox breath mask and treat oxygen as a dangerous atmosphere
	with Rating 8
- May survive in vacuum for up to five minutes before suffering its effects

## Mirialan
**Wound Threshold** - 11 + Brawn

**Strain Threshold** - 10 + Willpower

**Starting XP** - 100

### Characteristics
- Brawn - 2
- Agility - 3
- Intellect - 2
- Cunning - 1
- Willpower - 2
- Presence - 2

### Special Abilities
- Start with one rank in Discipline skill
- Start with one rank in Cool skill

## Mon Calamari
**Wound Threshold** - 10 + Brawn

**Strain Threshold** - 10 + Willpower

**Starting XP** - 100

### Characteristics
- Brawn - 2
- Agility - 2
- Intellect - 3
- Cunning - 1
- Willpower - 2
- Presence - 2

### Special Abilities
- Start with one rank in Knowledge (Education) skill
- Can breathe underwater and do not suffer movement penalties in water

## Nautolan
**Wound Threshold** - 11 + Brawn

**Strain Threshold** - 9 + Willpower

**Starting XP** - 100

### Characteristics
- Brawn - 3
- Agility - 2
- Intellect - 2
- Cunning - 2
- Willpower - 1
- Presence - 2

### Special Abilities
- Start with one rank in Athletics skill
- Can breathe underwater and do not suffer movement penalties in water

## Rodian
**Wound Threshold** - 10 + Brawn

**Strain Threshold** - 10 + Willpower

**Starting XP** - 100

### Characteristics
- Brawn - 2
- Agility - 3
- Intellect - 2
- Cunning - 2
- Willpower - 1
- Presence - 2

### Special Abilities
- Start with one rank in Survival skill
- Start with one rank in Expert Tracker talent

## Sullustan
**Wound Threshold** - 10 + Brawn

**Strain Threshold** - 10 + Willpower

**Starting XP** - 100

### Characteristics
- Brawn - 2
- Agility - 3
- Intellect - 2
- Cunning - 1
- Willpower - 2
- Presence - 2

### Special Abilities
- Start with one rank in Astrogation skill
- Start with one rank in Skilled Jockey talent

## Togruta
**Wound Threshold** - 10 + Brawn

**Strain Threshold** - 10 + Willpower

**Starting XP** - 100

### Characteristics
- Brawn - 1
- Agility - 2
- Intellect - 2
- Cunning - 3
- Willpower - 2
- Presence - 2

### Special Abilities
- Start with one rank in Perception skill
- When performing the assist maneuver, grant 2 boost instead of 1

## Trandoshan
**Wound Threshold** - 12 + Brawn

**Strain Threshold** - 9 + Willpower

**Starting XP** - 90

### Characteristics
- Brawn - 3
- Agility - 1
- Intellect - 2
- Cunning - 2
- Willpower - 2
- Presence - 2

### Special Abilities
- Start with one rank in Perception skill
- Recover one additional wound whenever recover 1+ wounds from rest or bacta
- Regrow limbs (~1 month)
- Brawl checks deal +1 damage and have a Critical Rating of 3

## Twi'lek
**Wound Threshold** - 10 + Brawn

**Strain Threshold** - 11 + Willpower

**Starting XP** - 100

### Characteristics
- Brawn - 1
- Agility - 2
- Intellect - 2
- Cunning - 2
- Willpower - 2
- Presence - 3

### Special Abilities
- Start with one rank in either Charm or Deception skills
- When making skill checks, may remove one setback due to arid or hot
	environmental conditions

## Wookiee
**Wound Threshold** - 14 + Brawn

**Strain Threshold** - 8 + Willpower

**Starting XP** - 90

### Characteristics
- Brawn - 3
- Agility - 2
- Intellect - 2
- Cunning - 2
- Willpower - 1
- Presence - 2

### Special Abilities
- Start with one rank in Brawl skill
- When suffering any wounds, deal +1 damage to Brawl and Melee attacks
- When Critically Injured, instead deal +2 damage to Brawl and Melee attacks

## Zabrak
**Wound Threshold** - 10 + Brawn

**Strain Threshold** - 10 + Willpower

**Starting XP** - 100

### Characteristics
- Brawn - 2
- Agility - 2
- Intellect - 2
- Cunning - 2
- Willpower - 3
- Presence - 1

### Special Abilities
- Start with one rank in Survival skill
- Add automatic advantage to all Coercion checks
